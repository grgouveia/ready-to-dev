resource "azurerm_role_definition" "rtd-owner-role" {
  name               = "${var.resource-prefix}-owner-role"
  role_definition_id = "${data.azurerm_subscription.subscriptions.id}${data.azurerm_builtin_role_definition.owner.id}"
  principal_id       = "${lookup(azurerm_virtual_machine.test.identity[0], "principal_id")}"

  permissions {
    actions     = ["Microsoft.Resources/subscriptions/resourceGroups/read"]
    not_actions = []
  }

  assignable_scopes = [
    "${data.azurerm_subscription.current.id}",
  ]
}

resource "azurerm_role_assignment" "chef-rbac" {
    name               = "${azurerm_virtual_machine.chef-server.name}"
    scope              = "${data.azurerm_subscription.primary.id}"
    role_definition_id = "${data.azurerm_subscription.subscription.id}${ data.azurerm_builtin_role_definition.owner.id }"
    principal_id       = "${lookup(azurerm_virtual_machine.chef-server.identity[0], "principal_id")}"
}


resource "azurerm_role_assignment" "target-rbac" {
    name               = "${azurerm_virtual_machine.target.name}"
    scope              = "${data.azurerm_subscription.primary.id}"
    role_definition_id = "${data.azurerm_subscription.subscription.id}${ data.azurerm_builtin_role_definition.owner.id }"
    principal_id       = "${lookup(azurerm_virtual_machine.target.identity[0], "principal_id")}"

}
