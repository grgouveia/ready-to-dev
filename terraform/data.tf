data "azurerm_builtin_role_definition" "owner" {
  name = "Owner"
}

  # get the ssh keys
data "local_file" "sshpk" {
    # private key
    depends_on = [null_resource.sshkg]
    filename = "${var.file-path}${var.vm-name}"
}
data "local_file" "sshpub" {
    # public key
    depends_on = [null_resource.sshkg]
    filename = "${var.file-path}${var.vm-name}.pub"
}

data "azurerm_subscription" "current" {}

data "azurerm_client_config" "rtd" {}

data "azurerm_resource_group" "rg" {
  name = "ready-to-dev-rg"
}
