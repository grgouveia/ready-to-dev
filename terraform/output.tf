
output "network-interface-public-ip" {
    value = "${ azurerm_public_ip.nic-pubip.ip_address }"
}                              