variable "subscription_id" {}

variable "tenant_id" {}

variable "file-path" {
    default = "~/.ssh"

}

variable "vm-name" {}

variable "resource-prefix" {}

variable "vms_pwd" {}

variable "vms_username" {}

variable "location" {}

variable "vm-size" {}

variable "tags" {
    type = "map"
    default = {
        Environment = "dev"
        Project = "rtd"
    }
}

variable "sku" {
    type = "map"
    default = {
        BR = "18.04-LTS"
    }
}