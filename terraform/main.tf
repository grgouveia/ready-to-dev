
provider "azurerm" {
    version = "~>1.32.0"
    use_msi = true
    subscription_id = "14a96e72-01f9-4e08-b23a-ad780f8cc6d5"
    tenant_id       = "00ff4409-24f0-4a5c-8161-c8ad9ee26d9d"
}

# Create a resource group
resource "azurerm_resource_group" "rg" {
  name     = "ready-to-dev"
  location = "${var.location}"

  tags = {
    environment = "dev"
  }
}

# Create a virtual network within the resource group
resource "azurerm_virtual_network" "vnet" {
  name                = "rtd-vnet"
  resource_group_name = "${azurerm_resource_group.rg.name}"
  location            = azurerm_resource_group.rg.location
  address_space       = ["10.0.0.0/16"]
}

# Create subnet
resource "azurerm_subnet" "subnet" {
  name                 = "rtd-subnet"
  resource_group_name  = azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.vnet.name
  address_prefix       = "10.0.1.0/24"
}

# Create public IP
resource "azurerm_public_ip" "nic-pubip" {
  name                = "${var.resource-prefix}-nic-pubip"
  location            = var.location
  resource_group_name = azurerm_resource_group.rg.name
  allocation_method   = "Static"
}

# Create Network Security Group and rule
resource "azurerm_network_security_group" "nsg" {
  name                = "${var.resource-prefix}-sg"
  location            = "${var.location}"
  resource_group_name = azurerm_resource_group.rg.name

  security_rule {
    name                       = "SSH"
    priority                   = 1001
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}

# Create network interface
resource "azurerm_network_interface" "nic" {
  name                      = "${var.resource-prefix}"
  location                  = "${var.location}"
  resource_group_name       = azurerm_resource_group.rg.name
  network_security_group_id = azurerm_network_security_group.nsg.id

  ip_configuration {
    name                          = "${var.resource-prefix}-config"
    subnet_id                     = azurerm_subnet.subnet.id
    private_ip_address_allocation = "dynamic"
    public_ip_address_id          = azurerm_public_ip.nic-pubip.id
  }
}


# Create a Linux virtual machine
resource "azurerm_virtual_machine" "target" {
  name                  = "${var.resource-prefix}-target"
  location              = "${var.location}"
  resource_group_name   = azurerm_resource_group.rg.name
  network_interface_ids = [azurerm_network_interface.nic.id]
  vm_size               = "${var.vm-size}"

  storage_os_disk {
    name              = "${var.resource-prefix}-osDisk"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Premium_LRS"
  }

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "va.ubuntu-iso-version"
    version   = "lartd"
  }

  os_profile {
    computer_name  = "${var.resource-prefix}-target"
    admin_username = "${var.vms_username}"
    admin_password = "${var.vms_pwd}"
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }
}

resource "azurerm_virtual_machine" "chef-server" {
  name                  = "${var.resource-prefix}-chef-server"
  location              = "${var.location}"
  resource_group_name   = azurerm_resource_group.rg.name
  network_interface_ids = [azurerm_network_interface.nic.id]
  vm_size               = "${var.vm-size}"

  storage_os_disk {
    name              = "${var.resource-prefix}-osDisk"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Premium_LRS"
  }

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = lookup(var.sku, var.location)
    version   = "lartd"
  }

  os_profile {
    computer_name  = "${var.resource-prefix}-chef-server"
    admin_username = "${var.vms_username}"
    admin_password = "${var.vms_pwd}"
  }


  os_profile_linux_config {
    disable_password_authentication = false
  }

  connection {
      type     = "ssh"
      host     = azurerm_public_ip.nic-pubip.ip_address
      user     = "${var.vms_username}"
      password =  "${var.vms_pwd}"
  }

  # provisioner "file" {

  #     source      = "newfile.txt"
  #     destination = "newfile.txt"
  # }

  # provisioner "remote-exec" {
  #     inline = [
  #     "ls -a",
  #     "cat newfile.txt"
  #     ]
  # }
}