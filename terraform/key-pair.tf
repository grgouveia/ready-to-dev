
# get a passphrase that contains alpha, numeric, and some special characters, but no spaces
resource "random_string" "passphrase" {
    length = 12
    special = true
    override_special = "!@#$%&*-_=?"
    number = true
}

# generates key-pair
resource "null_resource" "sshkg" {
    provisioner "local-exec" {
        connection {
            type     = "ssh"
            host     = azurerm_public_ip.nic-pubip.ip_address
            user     = "${var.vms_username}"
            password =  "${var.vms_pwd}"
        }

        command = "ssh-keygen -f ${var.file-path}${var.vm-name} -t rsa -b 4096 -N ${random_string.passphrase.result}"
    }
}