# terraform {
#   backend "local" {
#     path = "terraform.tfstate"
#   }
# }

data "terraform_remote_state" "tf-cloud" {
  backend = "remote"

  config = {
    organization = "grgouveia"

    workspaces {
      name = "ready-to-dev"
    }
  }
}