#Script used to automate brand-new ubuntu setup. 
#It install entertainment and web apps if specified
#on prompt, such as spotify and chrome browser,
#otherwise only OS utility packages, as well, as
#developer's common tools and and developer's environment
#and additional drivers, such as Nviida. Read carefully the script
#if you intend to use it for your own purposes as it may have undesired effects.

#!/bin/bash -ex

  TOOLS_PATH=/tools
  DEB_PKGS=('postman' 
				 'code')
  DEB_PKGS_URL=('https://dl.pstmn.io/download/latest/linux64' 
						 'https://go.microsoft.com/fwlink/?LinkID=760868')
  USER_PACKAGES='spotify-client google-chrome-stable'
  OS_PACKAGES='xsel wget vim software-properties-common python3-pip'

  function addRepos() {
    echo "Adding Chrome Repository"
    wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
    sudo sh -c 'echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list'

    echo "Adding Spotify Repository"
    curl -sS https://download.spotify.com/debian/pubkey.gpg | sudo apt-key add - 
    echo "deb http://repository.spotify.com stable non-free" | sudo tee /etc/apt/sources.list.d/spotify.list
  }

  function downloadDebPkgs() {
    OLDPWD=$PWD
    cd /tmp

    len=${#DEB_PKGS[@]}
    for (( i=0; i<$len; i++ )); do 	
			file=${DEB_PKGS[$i]}
			echo "Verifying Existence of Deb Package: $file"
			if [ ! -e "$file.deb" ]; then
				url=${DEB_PKGS_URL[$i]}
		
				echo "Downloading $i packages"
				wget $url -O $file.deb
			fi
    done

    cd -
  }

	function installDebPkgs() {
    OLDPWD=$PWD
    cd /tmp

		echo "Installing .deb package: $1"
		sudo dpkg -i -y $1
	  xargs apt install -f -y $1
		
		cd -
	}

	function installDevTools() {
		OLDPWD=$PWD

		echo "Installing Virtual Box"
		cd /tmp
		curl -O https://download.virtualbox.org/virtualbox/6.0.14/virtualbox-6.0_6.0.14-133895~Ubuntu~bionic_amd64.deb
		sudo apt install ./virtualbox* -y
		cd -

		echo "Installing Vagrant"
		sudo apt install vagrant -y
	
		wget -q https://packages.microsoft.com/keys/microsoft.asc -O- | sudo apt-key add -
		sudo add-apt-repository "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main"	
		sudo apt update
		sudo apt install code
	}

  function installUserApps() {
		echo "Installing User Apps with GUI: $USER_PACKAGES"

		OLDPWD=$PWD

	 	addRepos
    updateApt

    sudo apt install -y $USER_PACKAGES

	  downloadDebPkgs
		find . -name "*.deb" | xargs -r installDebPkgs 
	}

  function updateApt() {
    sudo apt update -y && sudo apt upgrade -y 
  }

  function installAdditionalDrivers() {
    echo "Installing only recommended additional drivers"
    sudo ubuntu-drivers devices | grep recommended | awk '{print $3}' | xargs sudo apt install -y
  }

  function main() {
    read -p "Do you wish to reboot after setup? (Y/n): " REBOOT
    read -p "Install recommended additional drivers? (Y/n): " DRIVERS
		read -p "Run user-mode?: (Y/n) \n (Accepting this will install GUI softwares, unncessary for servers.): " USER_MODE
    echo "Initializing initial setup"
    updateApt
    sudo apt install -y $OS_PACKAGES

    if [ ! -d "$TOOLS_PATH" ]; then
      echo "Creating /tools dir for useful scripts centralization"

      mkdir -p $TOOLS_PATH
      #sudo chown "$(id -u):$(id -g)"
      sudo chown $USER:$USER $TOOLS_PATH 
      sudo chmod u+x $TOOLS_PATH/* && sudo chmod u+x $TOOLS_PATH/.*
      ln -s $BASH_SOURCE $TOOLS_PATH/setup
    fi
    
 		if [ "$USER_MODE" == "Y" ]; then
      installUserApps
    fi
   
		if [ "$DEV_MODE" == "Y" ]; then
			installDevTools
		fi

    if [ "$DRIVERS" == "Y" ]; then
      installAdditionalDrivers
		fi

    sudo apt autoremove -y

    if [ "$REBOOT" == "Y" ]; then
     sudo reboot -f now	
    fi
  }

  main
